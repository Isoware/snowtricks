<?php

namespace App\Controller;

use App\Entity\StCommentaire;
use App\Entity\StIllustration;
use App\Entity\StTrick;
use App\Form\CommentaireType;
use App\Form\EditTrickType;
use App\Form\TrickType;
use App\Repository\StCommentaireRepository;
use App\Repository\StIllustrationRepository;
use App\Repository\StTrickRepository;
use App\Repository\StUtilisateurRepository;
use App\Service\FileUploader;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TricksController extends AbstractController
{
    /**
     * @Route("/tricks/{slug}-{id}", name="tricks.show", requirements={"slug": "[a-z0-9\-]+"})
     * @param StTrick $trick
     * @param string $slug
     * @param StUtilisateurRepository $userRepo
     * @param Request $request
     * @return Response
     */
    public function show(StTrick $trick, string $slug, StUtilisateurRepository $userRepo, Request $request): Response
    {
        if ($trick->getSlug() !== $slug) {
            return $this->redirectToRoute('tricks.show', [
                'id' => $trick->getId(),
                'slug' => $trick->getSlug()
            ], 301);
        }

        $commentaire = new StCommentaire();

        $form = $this->createForm(CommentaireType::class, $commentaire);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager = $this->getDoctrine()->getManager();
            $commentaire->setTrick($trick);
            $commentaire->setUtilisateur($userRepo->loadActualUser($this->getUser()));

            $manager->persist($commentaire);
            $manager->flush();

            return $this->redirectToRoute("tricks.show", [
                'id' => $trick->getId(),
                'slug' => $trick->getSlug()
            ]);
        }

        return $this->render("tricks/show.html.twig", [
            'trick' => $trick,
            'commentForm' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param StUtilisateurRepository $userRepo
     * @return Response
     *
     * @throws NonUniqueResultException
     * @Route("/tricks/creation", name="tricks.create")
     */
    public function create(Request $request, StUtilisateurRepository $userRepo): Response
    {
        $trick = new StTrick();

        $form = $this->createForm(TrickType::class, $trick);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager = $this->getDoctrine()->getManager();

            $trick->setUtilisateur($userRepo->loadActualUser($this->getUser()));
            $manager->persist($trick);

            $manager->flush();

            if($form->get('createThenMedia')->isClicked()) {
                return $this->redirectToRoute("tricks.addMedia", [
                    'id' => $trick->getId(),
                    'slug' => $trick->getSlug()
                ]);
            }

            return $this->redirectToRoute("home");
        }

        return $this->render('tricks/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/tricks/delete/{id}", name="tricks.delete")
     * @param StTrick $trick
     * @return Response
     */
    public function delete(StTrick $trick): Response
    {
        $this->denyAccessUnlessGranted('ROLE_EDITOR');

        $manager = $this->getDoctrine()->getManager();
        $manager->remove($trick);
        $manager->flush();

        return $this->redirectToRoute("home");
    }

    /**
     * @Route("/tricks/addMedia/{slug}-{id}", name="tricks.addMedia", requirements={"slug": "[a-z0-9\-]+"})
     * @param Request $request
     * @param StTrick $trick
     * @param string $slug
     * @param FileUploader $fileUploader
     * @return RedirectResponse|Response
     */
    public function addMedia(Request $request, StTrick $trick, string $slug, FileUploader $fileUploader): RedirectResponse|Response
    {
        if ($trick->getSlug() !== $slug) {
            return $this->redirectToRoute('tricks.addMedia', [
                'id' => $trick->getId(),
                'slug' => $trick->getSlug()
            ], 301);
        }

        if ($request->getMethod() == "POST") {
            $manager = $this->getDoctrine()->getManager();

            $file = new UploadedFile($_FILES['file']['tmp_name'], $_FILES['file']['name']);

            $illustrationFileName = $fileUploader->upload($file, 'images');

            $illustration = new StIllustration();
            $illustration->setTrick($trick);
            $illustration->setUrl($illustrationFileName);

            $manager->persist($illustration);
            $manager->flush();
        }

        return $this->render("tricks/addMedia.html.twig", [
            'trick' => $trick
        ]);
    }

    /**
     * @Route("/tricks/setFavorite/{id}", name="tricks.setFavorite")
     * @param Request $request
     * @param StTrick $trick
     * @param StIllustrationRepository $illustration_rep
     * @return Response
     */
    public function setFavorites(Request $request, StTrick $trick, StIllustrationRepository $illustration_rep): Response
    {
        $this->denyAccessUnlessGranted('ROLE_EDITOR');
        $illustration_id = $request->request->get('illustration-id');
        $illustration = $illustration_rep->find($illustration_id);

        $manager = $this->getDoctrine()->getManager();
        $trick->setFavoriteIllustration($illustration);
        $manager->persist($trick);
        $manager->flush();

        return new Response('200');
    }

    /**
     * @Route("/tricks/modification/{slug}-{id}", name="tricks.modify", requirements={"slug": "[a-z0-9\-]+"})
     * @param Request $request
     * @param StTrick $trick
     * @param string $slug
     * @return RedirectResponse|Response
     */
    public function modify(Request $request, StTrick $trick, string $slug): RedirectResponse|Response
    {
        if ($trick->getSlug() !== $slug) {
            return $this->redirectToRoute('tricks.addMedia', [
                'id' => $trick->getId(),
                'slug' => $trick->getSlug()
            ], 301);
        }

        $form = $this->createForm(EditTrickType::class, $trick);

        $form->handleRequest($request);

        if ($request->getMethod() == "POST") {
            $manager = $this->getDoctrine()->getManager();

            $manager->persist($trick);
            $manager->flush();

            return $this->redirectToRoute('tricks.show', [
                'id' => $trick->getId(),
                'slug' => $trick->getSlug()
            ], 301);
        }

        return $this->render("tricks/create.html.twig", [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/tricks/deleteIllustration", name="tricks.deleteIllustration",)
     */
    public function deleteIllustration(Request $request, StIllustrationRepository $illustrationRep, StTrickRepository $trickRepo)
    {
        $this->denyAccessUnlessGranted('ROLE_EDITOR');

        $illustrationId = $request->request->get('illustrationId');
        $illustration = $illustrationRep->find($illustrationId);

        $manager = $this->getDoctrine()->getManager();

        if($request->request->get('favorite')) {
            $trick = $trickRepo->find($illustration->getTrick());
            $trick->setFavoriteIllustration(null);
            $manager->persist($trick);
        }

        $manager->remove($illustration);
        $manager->flush();

        return new Response('200');
    }
}
