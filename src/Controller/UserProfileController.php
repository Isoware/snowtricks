<?php

namespace App\Controller;

use App\Entity\StUtilisateur;
use App\Form\EditProfileType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserProfileController extends AbstractController
{
    /**
     * @param StUtilisateur $user
     * @param Request $request
     * @param UserInterface $actualUser
     * @return Response
     * @Route("/profil/{username}", name="user.profile")
     */
    public function userProfile(StUtilisateur $user, Request $request, UserInterface $actualUser): Response
    {
        if($actualUser->getUsername() !== $user->getUsername()) {
            return $this->redirectToRoute("home");
        }

        $form = $this->createForm(EditProfileType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('message', 'Profil modifié avec succès.');
        }

        return $this->render('profile/userProfile.html.twig', [
            'current_page' => 'profil',
            'form' => $form->createView()
        ]);
    }

    /**
     * @param StUtilisateur $user
     * @param Request $request
     * @param UserInterface $actualUser
     * @param UserPasswordEncoderInterface $upei
     * @return Response
     * @Route("/profil/{username}/modifierMotDePasse", name="user.profile.editPass")
     */
    public function editUserPass(StUtilisateur $user, Request $request, UserInterface $actualUser, UserPasswordEncoderInterface $upei): Response
    {
        if($actualUser->getUsername() !== $user->getUsername()) {
            return $this->redirectToRoute("home");
        }

        if($request->isMethod('POST')) {
            $em = $this->getDoctrine()->getManager();

            if($request->request->get('newPass') === $request->request->get('confirmPass')) {
                if(!$user->checkPassValidity($request->request->get('newPass'))) {
                    $this->addFlash('error', 'Le mot de passe doit faire plus de 8 caractères et contenir au moins 1 majuscule, 1 minuscule et 1 chiffre.');
                } else {
                    $user->setPassword($upei->encodePassword($user, $request->request->get('newPass')));
                    $em->flush();
                    $this->addFlash('success', 'Mot de passe modifié avec succès.');

                    return $this->redirectToRoute("user.profile", [
                        "username" => $actualUser->getUsername()
                    ]);
                }
            } else {
                $this->addFlash('error', 'Les mots de passe ne sont pas identiques');
            }
        }

        return $this->render('profile/editPass.html.twig', [
            'current_page' => 'profil'
        ]);
    }
}
