<?php

namespace App\Controller;

use App\Entity\StUtilisateur;
use App\Form\UserType;
use App\Repository\StUtilisateurRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * @package App\Controller
 * @Route("/admin", name="admin.")
 */
class AdminController extends AbstractController
{
    /**
     * @param StUtilisateurRepository $users
     * @return Response
     * @Route("/utilisateurs", name="users")
     */
    public function users(StUtilisateurRepository $users): Response
    {
        return $this->render("admin/users.html.twig", [
            'current_page' => 'admin',
            'users' => $users->findAll()
        ]);
    }

    /**
     * @param StUtilisateur $user
     * @param Request $request
     * @return RedirectResponse|Response
     * @Route("/utilisateur/modifier/{id}", name="edit.user")
     */
    public function editUser(StUtilisateur $user, Request $request): RedirectResponse|Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('message', 'Utilisateur modifié avec succès.');
            return $this->redirectToRoute('admin.users');
        }

        return $this->render('admin/editUser.html.twig', [
            'current_page' => 'admin',
            'form' => $form->createView()
        ]);
    }

    /**
     * @param StUtilisateur $user
     * @return RedirectResponse
     * @Route("/utilisateur/delete/{id}", name="delete.user")
     */
    public function deleteUser(StUtilisateur $user): RedirectResponse
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('admin.users');
    }
}
