<?php

namespace App\Controller;

use App\Repository\StTrickRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @param StTrickRepository $trickRepository
     * @return Response
     */
    public function index(StTrickRepository $trickRepository): Response
    {
        $tricks = $trickRepository->findLatest();
        return $this->render('pages/home.html.twig', [
            "tricks" => $tricks
        ]);
    }

}
