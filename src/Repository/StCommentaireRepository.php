<?php

namespace App\Repository;

use App\Entity\StCommentaire;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StCommentaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method StCommentaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method StCommentaire[]    findAll()
 * @method StCommentaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StCommentaireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StCommentaire::class);
    }

    // /**
    //  * @return StCommentaire[] Returns an array of StCommentaire objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StCommentaire
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
