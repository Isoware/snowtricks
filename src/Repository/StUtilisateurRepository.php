<?php

namespace App\Repository;

use App\Entity\StUtilisateur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method StUtilisateur|null find($id, $lockMode = null, $lockVersion = null)
 * @method StUtilisateur|null findOneBy(array $criteria, array $orderBy = null)
 * @method StUtilisateur[]    findAll()
 * @method StUtilisateur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StUtilisateurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StUtilisateur::class);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function loadActualUser(UserInterface $user)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.username = :username')
            ->andWhere('u.password = :password')
            ->setParameter('username', $user->getUsername())
            ->setParameter('password', $user->getPassword())
            ->getQuery()
            ->getOneOrNullResult();
    }

    // /**
    //  * @return StUtilisateur[] Returns an array of StUtilisateur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StUtilisateur
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
