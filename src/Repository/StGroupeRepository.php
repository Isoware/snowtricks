<?php

namespace App\Repository;

use App\Entity\StGroupe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StGroupe|null find($id, $lockMode = null, $lockVersion = null)
 * @method StGroupe|null findOneBy(array $criteria, array $orderBy = null)
 * @method StGroupe[]    findAll()
 * @method StGroupe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StGroupeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StGroupe::class);
    }

    // /**
    //  * @return StGroupe[] Returns an array of StGroupe objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StGroupe
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
