<?php

namespace App\Repository;

use App\Entity\StTrick;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StTrick|null find($id, $lockMode = null, $lockVersion = null)
 * @method StTrick|null findOneBy(array $criteria, array $orderBy = null)
 * @method StTrick[]    findAll()
 * @method StTrick[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StTrickRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StTrick::class);
    }

    /**
     * @return StTrick[]
     */
    public function findLatest(): array
    {
        return $this->createQueryBuilder('t')
            ->setMaxResults(15)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return StTrick[] Returns an array of StTrick objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StTrick
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
