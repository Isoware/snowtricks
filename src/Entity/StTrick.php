<?php

namespace App\Entity;

use App\Repository\StTrickRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Cocur\Slugify\Slugify;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=StTrickRepository::class)
 * @UniqueEntity(
 *     fields = {"nom"},
 *     message = "Ce nom est déjà utilisé"
 * )
 */
class StTrick
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\OneToMany(targetEntity=StIllustration::class, mappedBy="trick", orphanRemoval=true)
     */
    private $illustrations;

    /**
     * @ORM\OneToMany(targetEntity=StVideo::class, mappedBy="trick", orphanRemoval=true)
     */
    private $videos;

    /**
     * @ORM\OneToMany(targetEntity=StCommentaire::class, mappedBy="trick", orphanRemoval=true)
     */
    private $commentaires;

    /**
     * @ORM\ManyToOne(targetEntity=StUtilisateur::class, inversedBy="tricks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $utilisateur;

    /**
     * @ORM\ManyToOne(targetEntity=StGroupe::class, inversedBy="tricks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $groupe;

    /**
     * @ORM\OneToOne(targetEntity=StIllustration::class, cascade={"persist", "remove"})
     */
    private $favorite_illustration;

    public function __construct()
    {
        $this->date = new \DateTime();
        $this->illustrations = new ArrayCollection();
        $this->videos = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getSlug(): string
    {
        return (new Slugify())->slugify($this->nom);
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getIllustrations(): Collection
    {
        return $this->illustrations;
    }

    public function addIllustration(StIllustration $illustration): self
    {
        if (!$this->illustrations->contains($illustration)) {
            $this->illustrations[] = $illustration;
            $illustration->setIdTrick($this);
        }

        return $this;
    }

    public function removeIllustration(StIllustration $illustration): self
    {
        if ($this->illustrations->removeElement($illustration)) {
            // set the owning side to null (unless already changed)
            if ($illustration->getIdTrick() === $this) {
                $illustration->setIdTrick(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StVideo[]
     */
    public function getVideos(): Collection
    {
        return $this->videos;
    }

    public function addVideo(StVideo $video): self
    {
        if (!$this->videos->contains($video)) {
            $this->videos[] = $video;
            $video->setIdTrick($this);
        }

        return $this;
    }

    public function removeVideo(StVideo $video): self
    {
        if ($this->videos->removeElement($video)) {
            // set the owning side to null (unless already changed)
            if ($video->getIdTrick() === $this) {
                $video->setIdTrick(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StCommentaire[]
     */
    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    public function addCommentaire(StCommentaire $commentaire): self
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->setIdTrick($this);
        }

        return $this;
    }

    public function removeCommentaire(StCommentaire $commentaire): self
    {
        if ($this->commentaires->removeElement($commentaire)) {
            // set the owning side to null (unless already changed)
            if ($commentaire->getIdTrick() === $this) {
                $commentaire->setIdTrick(null);
            }
        }

        return $this;
    }

    public function getUtilisateur(): ?StUtilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?StUtilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    public function getGroupe(): ?StGroupe
    {
        return $this->groupe;
    }

    public function setGroupe(?StGroupe $groupe): self
    {
        $this->groupe = $groupe;

        return $this;
    }

    public function getFavoriteIllustration(): ?StIllustration
    {
        return $this->favorite_illustration;
    }

    public function setFavoriteIllustration(?StIllustration $favorite_illustration): self
    {
        $this->favorite_illustration = $favorite_illustration;

        return $this;
    }
}
