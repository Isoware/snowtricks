<?php

namespace App\Entity;

use App\Repository\StIllustrationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StIllustrationRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class StIllustration
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity=StTrick::class, inversedBy="illustrations")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $trick;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getTrick(): ?StTrick
    {
        return $this->trick;
    }

    public function setTrick(?StTrick $trick): self
    {
        $this->trick = $trick;

        return $this;
    }

    /**
    +     * @ORM\PostRemove
    +     */
    public function removeUpload()
    {
        $fileUrl = __DIR__ . '/../../public/media/images/' . $this->getUrl();
        if(file_exists($fileUrl)) {
            unlink($fileUrl);
        }
    }

}
