<?php

namespace App\Entity;

use App\Repository\StGroupeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StGroupeRepository::class)
 */
class StGroupe
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity=StTrick::class, mappedBy="groupe")
     */
    private $tricks;

    public function __construct()
    {
        $this->stTricks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|StTrick[]
     */
    public function getTricks(): Collection
    {
        return $this->tricks;
    }

    public function addTricks(StTrick $tricks): self
    {
        if (!$this->tricks->contains($tricks)) {
            $this->tricks[] = $tricks;
            $tricks->setIdGroupe($this);
        }

        return $this;
    }

    public function removeTricks(StTrick $trick): self
    {
        if ($this->tricks->removeElement($trick)) {
            // set the owning side to null (unless already changed)
            if ($trick->getGroupe() === $this) {
                $trick->setGroupe(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getNom();
    }
}
