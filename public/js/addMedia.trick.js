$(function() {
    $('#addVideoInput').on('click', (e) => {
        e.preventDefault();
        $('#videoInputs').append("<input type=\"text\" name=\"video[]\" maxlength=\"255\" class=\"form-control\">");
    });

    $('#file').on('change', function() {
        document.forms[0].submit();
    });

});
