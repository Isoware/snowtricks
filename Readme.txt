1) Installer Wamp
2) Importer "jeuDeDonnees.sql" dans PhpMyAdmin
3) Renseigner la variable DATABASE_URL dans le .env
4) Renseigner la variable MAILER_DSN dans le .env
5) Lancer la commande "symfony serve"
6) Aller sur l'adresse indiquée par la console