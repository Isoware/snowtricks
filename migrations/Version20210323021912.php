<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210323021912 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE st_commentaire (id INT AUTO_INCREMENT NOT NULL, id_utilisateur_id INT NOT NULL, id_trick_id INT NOT NULL, contenu LONGTEXT NOT NULL, date DATETIME NOT NULL, INDEX IDX_7F60E566C6EE5C49 (id_utilisateur_id), INDEX IDX_7F60E566E25A52BB (id_trick_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE st_droit (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE st_illustration (id INT AUTO_INCREMENT NOT NULL, id_trick_id INT NOT NULL, url VARCHAR(255) NOT NULL, INDEX IDX_B0653105E25A52BB (id_trick_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE st_role (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE st_role_droits (id INT AUTO_INCREMENT NOT NULL, id_droit_id INT NOT NULL, id_role_id INT NOT NULL, INDEX IDX_B41FB8A5A72DFE5 (id_droit_id), INDEX IDX_B41FB8A589E8BDC (id_role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE st_utilisateur (id INT AUTO_INCREMENT NOT NULL, id_role_id INT NOT NULL, mail VARCHAR(255) NOT NULL, mot_de_passe VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, INDEX IDX_58CEE6989E8BDC (id_role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE st_video (id INT AUTO_INCREMENT NOT NULL, id_trick_id INT NOT NULL, url VARCHAR(255) NOT NULL, INDEX IDX_18E72A31E25A52BB (id_trick_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE st_commentaire ADD CONSTRAINT FK_7F60E566C6EE5C49 FOREIGN KEY (id_utilisateur_id) REFERENCES st_utilisateur (id)');
        $this->addSql('ALTER TABLE st_commentaire ADD CONSTRAINT FK_7F60E566E25A52BB FOREIGN KEY (id_trick_id) REFERENCES st_trick (id)');
        $this->addSql('ALTER TABLE st_illustration ADD CONSTRAINT FK_B0653105E25A52BB FOREIGN KEY (id_trick_id) REFERENCES st_trick (id)');
        $this->addSql('ALTER TABLE st_role_droits ADD CONSTRAINT FK_B41FB8A5A72DFE5 FOREIGN KEY (id_droit_id) REFERENCES st_droit (id)');
        $this->addSql('ALTER TABLE st_role_droits ADD CONSTRAINT FK_B41FB8A589E8BDC FOREIGN KEY (id_role_id) REFERENCES st_role (id)');
        $this->addSql('ALTER TABLE st_utilisateur ADD CONSTRAINT FK_58CEE6989E8BDC FOREIGN KEY (id_role_id) REFERENCES st_role (id)');
        $this->addSql('ALTER TABLE st_video ADD CONSTRAINT FK_18E72A31E25A52BB FOREIGN KEY (id_trick_id) REFERENCES st_trick (id)');
        $this->addSql('ALTER TABLE st_trick CHANGE date date DATETIME NOT NULL, CHANGE id_groupe id_groupe_id INT NOT NULL');
        $this->addSql('ALTER TABLE st_trick ADD CONSTRAINT FK_BCD05903FA7089AB FOREIGN KEY (id_groupe_id) REFERENCES st_groupe (id)');
        $this->addSql('CREATE INDEX IDX_BCD05903FA7089AB ON st_trick (id_groupe_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE st_role_droits DROP FOREIGN KEY FK_B41FB8A5A72DFE5');
        $this->addSql('ALTER TABLE st_role_droits DROP FOREIGN KEY FK_B41FB8A589E8BDC');
        $this->addSql('ALTER TABLE st_utilisateur DROP FOREIGN KEY FK_58CEE6989E8BDC');
        $this->addSql('ALTER TABLE st_commentaire DROP FOREIGN KEY FK_7F60E566C6EE5C49');
        $this->addSql('DROP TABLE st_commentaire');
        $this->addSql('DROP TABLE st_droit');
        $this->addSql('DROP TABLE st_illustration');
        $this->addSql('DROP TABLE st_role');
        $this->addSql('DROP TABLE st_role_droits');
        $this->addSql('DROP TABLE st_utilisateur');
        $this->addSql('DROP TABLE st_video');
        $this->addSql('ALTER TABLE st_trick DROP FOREIGN KEY FK_BCD05903FA7089AB');
        $this->addSql('DROP INDEX IDX_BCD05903FA7089AB ON st_trick');
        $this->addSql('ALTER TABLE st_trick CHANGE date date DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, CHANGE id_groupe_id id_groupe INT NOT NULL');
    }
}
