<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210324225035 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE st_role_droits ADD CONSTRAINT FK_B41FB8A55AA93370 FOREIGN KEY (droit_id) REFERENCES st_droit (id)');
        $this->addSql('ALTER TABLE st_role_droits ADD CONSTRAINT FK_B41FB8A5D60322AC FOREIGN KEY (role_id) REFERENCES st_role (id)');
        $this->addSql('CREATE INDEX IDX_B41FB8A55AA93370 ON st_role_droits (droit_id)');
        $this->addSql('CREATE INDEX IDX_B41FB8A5D60322AC ON st_role_droits (role_id)');
        $this->addSql('ALTER TABLE st_trick DROP FOREIGN KEY FK_BCD05903FA7089AB');
        $this->addSql('DROP INDEX IDX_BCD05903FA7089AB ON st_trick');
        $this->addSql('ALTER TABLE st_trick ADD groupe_id INT NOT NULL, DROP id_groupe_id, DROP id_utilisateur');
        $this->addSql('ALTER TABLE st_trick ADD CONSTRAINT FK_BCD059037A45358C FOREIGN KEY (groupe_id) REFERENCES st_groupe (id)');
        $this->addSql('CREATE INDEX IDX_BCD059037A45358C ON st_trick (groupe_id)');
        $this->addSql('ALTER TABLE st_utilisateur DROP FOREIGN KEY FK_58CEE6989E8BDC');
        $this->addSql('DROP INDEX IDX_58CEE6989E8BDC ON st_utilisateur');
        $this->addSql('ALTER TABLE st_utilisateur ADD role_id INT DEFAULT NULL, DROP id_role_id');
        $this->addSql('ALTER TABLE st_utilisateur ADD CONSTRAINT FK_58CEE69D60322AC FOREIGN KEY (role_id) REFERENCES st_role (id)');
        $this->addSql('CREATE INDEX IDX_58CEE69D60322AC ON st_utilisateur (role_id)');
        $this->addSql('ALTER TABLE st_video DROP FOREIGN KEY FK_18E72A31E25A52BB');
        $this->addSql('DROP INDEX IDX_18E72A31E25A52BB ON st_video');
        $this->addSql('ALTER TABLE st_video ADD trick_id INT DEFAULT NULL, DROP id_trick_id');
        $this->addSql('ALTER TABLE st_video ADD CONSTRAINT FK_18E72A31B281BE2E FOREIGN KEY (trick_id) REFERENCES st_trick (id)');
        $this->addSql('CREATE INDEX IDX_18E72A31B281BE2E ON st_video (trick_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE st_role_droits DROP FOREIGN KEY FK_B41FB8A55AA93370');
        $this->addSql('ALTER TABLE st_role_droits DROP FOREIGN KEY FK_B41FB8A5D60322AC');
        $this->addSql('DROP INDEX IDX_B41FB8A55AA93370 ON st_role_droits');
        $this->addSql('DROP INDEX IDX_B41FB8A5D60322AC ON st_role_droits');
        $this->addSql('ALTER TABLE st_trick DROP FOREIGN KEY FK_BCD059037A45358C');
        $this->addSql('DROP INDEX IDX_BCD059037A45358C ON st_trick');
        $this->addSql('ALTER TABLE st_trick ADD id_utilisateur INT NOT NULL, CHANGE groupe_id id_groupe_id INT NOT NULL');
        $this->addSql('ALTER TABLE st_trick ADD CONSTRAINT FK_BCD05903FA7089AB FOREIGN KEY (id_groupe_id) REFERENCES st_groupe (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_BCD05903FA7089AB ON st_trick (id_groupe_id)');
        $this->addSql('ALTER TABLE st_utilisateur DROP FOREIGN KEY FK_58CEE69D60322AC');
        $this->addSql('DROP INDEX IDX_58CEE69D60322AC ON st_utilisateur');
        $this->addSql('ALTER TABLE st_utilisateur ADD id_role_id INT NOT NULL, DROP role_id');
        $this->addSql('ALTER TABLE st_utilisateur ADD CONSTRAINT FK_58CEE6989E8BDC FOREIGN KEY (id_role_id) REFERENCES st_role (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_58CEE6989E8BDC ON st_utilisateur (id_role_id)');
        $this->addSql('ALTER TABLE st_video DROP FOREIGN KEY FK_18E72A31B281BE2E');
        $this->addSql('DROP INDEX IDX_18E72A31B281BE2E ON st_video');
        $this->addSql('ALTER TABLE st_video ADD id_trick_id INT NOT NULL, DROP trick_id');
        $this->addSql('ALTER TABLE st_video ADD CONSTRAINT FK_18E72A31E25A52BB FOREIGN KEY (id_trick_id) REFERENCES st_trick (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_18E72A31E25A52BB ON st_video (id_trick_id)');
    }
}
