<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210419000226 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE st_role_droits DROP FOREIGN KEY FK_B41FB8A55AA93370');
        $this->addSql('ALTER TABLE st_role_droits DROP FOREIGN KEY FK_B41FB8A5D60322AC');
        $this->addSql('ALTER TABLE st_utilisateur DROP FOREIGN KEY FK_58CEE69D60322AC');
        $this->addSql('DROP TABLE st_droit');
        $this->addSql('DROP TABLE st_role');
        $this->addSql('DROP TABLE st_role_droits');
        $this->addSql('DROP INDEX IDX_58CEE69D60322AC ON st_utilisateur');
        $this->addSql('ALTER TABLE st_utilisateur ADD roles JSON DEFAULT NULL, DROP role_id');
        $this->addSql('ALTER TABLE st_utilisateur DROP role_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE st_droit (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE st_role (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE st_role_droits (id INT AUTO_INCREMENT NOT NULL, droit_id INT NOT NULL, role_id INT NOT NULL, INDEX IDX_B41FB8A55AA93370 (droit_id), INDEX IDX_B41FB8A5D60322AC (role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE st_role_droits ADD CONSTRAINT FK_B41FB8A55AA93370 FOREIGN KEY (droit_id) REFERENCES st_droit (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE st_role_droits ADD CONSTRAINT FK_B41FB8A5D60322AC FOREIGN KEY (role_id) REFERENCES st_role (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE st_utilisateur ADD role_id INT DEFAULT NULL, DROP roles');
        $this->addSql('ALTER TABLE st_utilisateur ADD CONSTRAINT FK_58CEE69D60322AC FOREIGN KEY (role_id) REFERENCES st_role (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_58CEE69D60322AC ON st_utilisateur (role_id)');
        $this->addSql('ALTER TABLE st_utilisateur ADD role_id');
    }
}
