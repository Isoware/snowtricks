<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210817173112 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE st_trick DROP FOREIGN KEY FK_BCD05903DC4F0D4D');
        $this->addSql('ALTER TABLE st_trick ADD CONSTRAINT FK_BCD05903DC4F0D4D FOREIGN KEY (favorite_illustration_id) REFERENCES st_illustration (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE st_trick DROP FOREIGN KEY FK_BCD05903DC4F0D4D');
        $this->addSql('ALTER TABLE st_trick ADD CONSTRAINT FK_BCD05903DC4F0D4D FOREIGN KEY (favorite_illustration_id) REFERENCES st_illustration (id) ON UPDATE NO ACTION ON DELETE CASCADE');
    }
}
