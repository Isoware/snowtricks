<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210729200802 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE st_trick ADD favorite_illustration_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE st_trick ADD CONSTRAINT FK_BCD05903DC4F0D4D FOREIGN KEY (favorite_illustration_id) REFERENCES st_illustration (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BCD05903DC4F0D4D ON st_trick (favorite_illustration_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE st_trick DROP FOREIGN KEY FK_BCD05903DC4F0D4D');
        $this->addSql('DROP INDEX UNIQ_BCD05903DC4F0D4D ON st_trick');
        $this->addSql('ALTER TABLE st_trick DROP favorite_illustration_id');
    }
}
